defmodule Manganese.RenderKitTest.Structs.OrthographicProjectionSetTest do
  use ExUnit.Case

  alias Manganese.CoreKit
  alias Manganese.RenderKit.Structs

  doctest Manganese.RenderKit.Structs.OrthographicProjectionSet

  # Deserialization
  describe "`Manganese.RenderKit.Structs.OrthographicProjectionSet.from_map/1`" do
    @tag orthographic_projection_set: true
    test "it deserializes an orthographic projection set from a map" do
      north_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/north.png"
      east_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/east.png"
      south_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/south.png"
      west_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/west.png"

      map = %{
        "north" => CoreKit.Structs.Link.to_map(north_link),
        "east" => CoreKit.Structs.Link.to_map(east_link),
        "south" => CoreKit.Structs.Link.to_map(south_link),
        "west" => CoreKit.Structs.Link.to_map(west_link)
      }
      projection_set = Structs.OrthographicProjectionSet.from_map map

      assert projection_set.north == north_link
      assert projection_set.east == east_link
      assert projection_set.south == south_link
      assert projection_set.west == west_link
    end
  end

  # Serialization
  describe "`Manganese.RenderKit.Structs.OrthographicProjectionSet.to_map/1`" do
    @tag orthographic_projection_set: true
    test "it serializes an orthographic projection set to a map" do
      north_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/north.png"
      east_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/east.png"
      south_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/south.png"
      west_link = CoreKit.Structs.Link.from_uri "http://render-kit.mangane.se/orthographic-projection-set/west.png"

      projection_set = %Structs.OrthographicProjectionSet{
        north: north_link,
        east: east_link,
        south: south_link,
        west: west_link
      }
      map = Structs.OrthographicProjectionSet.to_map projection_set

      assert CoreKit.Structs.Link.from_map(map["north"]) == north_link
      assert CoreKit.Structs.Link.from_map(map["east"]) == east_link
      assert CoreKit.Structs.Link.from_map(map["south"]) == south_link
      assert CoreKit.Structs.Link.from_map(map["west"]) == west_link
    end
  end
end
