defmodule Manganese.RenderKit.Structs.PointLightParameters do
  @moduledoc """

  """

  alias Manganese.SerializationKit
  alias Manganese.RenderKit.Structs

  @typedoc """

  """
  @type t :: %Structs.PointLightParameters{
    color: SerializationKit.Structs.UnityColor.t,
    intensity: float,
    relative_position: SerializationKit.Structs.UnityVector3.t
  }
  defstruct [
    :color,
    :intensity,
    :relative_position
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "color" => color,
    "intensity" => intensity,
    "relative_position" => relative_position
  }) do
    %Structs.PointLightParameters{
      color: SerializationKit.Structs.UnityColor.from_map(color),
      intensity: intensity,
      relative_position: SerializationKit.Structs.UnityVector3.from_map(relative_position)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.PointLightParameters{
    color: color,
    intensity: intensity,
    relative_position: relative_position
  }) do
    %{
      "color" => SerializationKit.Structs.UnityColor.to_map(color),
      "intensity" => intensity,
      "relative_position" => SerializationKit.Structs.UnityVector3.to_map(relative_position)
    }
  end
end