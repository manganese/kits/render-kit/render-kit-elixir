defmodule Manganese.RenderKit.Structs.RenderSet do
  @moduledoc """

  """

  alias Manganese.CoreKit
  alias Manganese.RenderKit.Structs

  @typedoc """

  """
  @type t :: %Structs.RenderSet{
    rendered: CoreKit.Structs.Link.t,
    diffuse: CoreKit.Structs.Link.t,
    normal: CoreKit.Structs.Link.t,
    depth: CoreKit.Structs.Link.t
  }
  defstruct [
    :rendered,
    :diffuse,
    :normal,
    :depth
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "rendered" => rendered,
    "diffuse" => diffuse,
    "normal" => normal,
    "depth" => depth
  }) do
    %Structs.RenderSet{
      rendered: CoreKit.Structs.Link.from_map(rendered),
      diffuse: CoreKit.Structs.Link.from_map(diffuse),
      normal: CoreKit.Structs.Link.from_map(normal),
      depth: CoreKit.Structs.Link.from_map(depth)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.RenderSet{
    rendered: rendered,
    diffuse: diffuse,
    normal: normal,
    depth: depth
  }) do
    %{
      "rendered" => CoreKit.Structs.Link.to_map(rendered),
      "diffuse" => CoreKit.Structs.Link.to_map(diffuse),
      "normal" => CoreKit.Structs.Link.to_map(normal),
      "depth" => CoreKit.Structs.Link.to_map(depth)
    }
  end
end