defmodule Manganese.RenderKit.Structs.Render do
  @moduledoc """

  """

  alias Manganese.CoreKit
  alias Manganese.RenderKit.Structs

  @typedoc """

  """
  @type t :: %Structs.Render{
    rendered: CoreKit.Structs.Link.t
  }
  defstruct [
    :rendered
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "rendered" => rendered
  }) do
    %Structs.Render{
      rendered: CoreKit.Structs.Link.from_map(rendered)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.Render{
    rendered: rendered
  }) do
    %{
      "rendered" => CoreKit.Structs.Link.to_map(rendered)
    }
  end
end