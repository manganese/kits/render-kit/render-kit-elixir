defmodule Manganese.RenderKit.Structs.RenderParameters do
  @moduledoc """

  """

  alias Manganese.SerializationKit
  alias Manganese.RenderKit.Structs

  @typedoc """

  """
  @type t :: %Structs.RenderParameters{
    # Camera transform
    relative_camera_position: SerializationKit.Structs.UnityVector3.t,
    relative_camera_rotation: SerializationKit.Structs.UnityQuaternion.t,

    # Ambient lighting
    ambient_sky_color: SerializationKit.Structs.UnityColor.t,
    ambient_equator_color: SerializationKit.Structs.UnityColor.t,
    ambient_ground_color: SerializationKit.Structs.UnityColor.t,

    # Directional lights
    directional_lights: MapSet.t(Structs.DirectionalLightParameters.t),

    # Point lights
    point_lights: MapSet.t(Structs.PointLightParameters.t)
  }
  defstruct [
    :relative_camera_position,
    :relative_camera_rotation,
    :ambient_sky_color,
    :ambient_equator_color,
    :ambient_ground_color,
    :directional_lights,
    :point_lights
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "relative_camera_position" => relative_camera_position,
    "relative_camera_rotation" => relative_camera_rotation,
    "ambient_sky_color" => ambient_sky_color,
    "ambient_equator_color" => ambient_equator_color,
    "ambient_ground_color" => ambient_ground_color,
    "directional_lights" => directional_lights,
    "point_lights" => point_lights
  }) do
    %Structs.RenderParameters{
      relative_camera_position: SerializationKit.Structs.UnityVector3.from_map(relative_camera_position),
      relative_camera_rotation: SerializationKit.Structs.UnityQuaternion.from_map(relative_camera_rotation),
      ambient_sky_color: SerializationKit.Structs.UnityColor.from_map(ambient_sky_color),
      ambient_equator_color: SerializationKit.Structs.UnityColor.from_map(ambient_equator_color),
      ambient_ground_color: SerializationKit.Structs.UnityColor.from_map(ambient_ground_color),
      directional_lights:
        directional_lights
        |> Enum.map(&Structs.DirectionalLightParameters.from_map/1),
      point_lights:
        point_lights
        |> Enum.map(&Structs.PointLightParameters.from_map/1)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.RenderParameters{
    relative_camera_position: relative_camera_position,
    relative_camera_rotation: relative_camera_rotation,
    ambient_sky_color: ambient_sky_color,
    ambient_equator_color: ambient_equator_color,
    ambient_ground_color: ambient_ground_color,
    directional_lights: directional_lights,
    point_lights: point_lights
  }) do
    %{
      "relative_camera_position" => SerializationKit.Structs.UnityVector3.to_map(relative_camera_position),
      "relative_camera_rotation" => SerializationKit.Structs.UnityQuaternion.to_map(relative_camera_rotation),
      "ambient_sky_color" => SerializationKit.Structs.UnityColor.to_map(ambient_sky_color),
      "ambient_equator_color" => SerializationKit.Structs.UnityColor.to_map(ambient_equator_color),
      "ambient_ground_color" => SerializationKit.Structs.UnityColor.to_map(ambient_ground_color),
      "directional_lights" =>
        directional_lights
        |> Enum.map(&Structs.DirectionalLightParameters.to_map/1)
        |> MapSet.to_list,
      "point_lights" =>
        point_lights
        |> Enum.map(&Structs.PointLightParameters.to_map/1)
        |> MapSet.to_list
    }
  end


  # Ecto type
  @behaviour Ecto.Type

  @impl Ecto.Type
  def type, do: :map

  @impl Ecto.Type
  def cast(%Structs.RenderParameters{} = render_parameters), do: { :ok, render_parameters }
  def cast(%{} = map), do: { :ok, from_map map }
  def cast(nil), do: { :ok, nil }
  def cast(_), do: :error

  @impl Ecto.Type
  def load(%{} = map), do: { :ok, from_map map }
  def load(nil), do: { :ok, nil }
  def load(_), do: :error

  @impl Ecto.Type
  def dump(%Structs.RenderParameters{} = render_parameters), do: { :ok, to_map render_parameters }
  def dump(nil), do: { :ok, nil }
  def dump(_), do: :error
end