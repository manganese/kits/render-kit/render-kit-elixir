defmodule Manganese.RenderKit.Structs.OrthographicProjectionSet do
  @moduledoc """

  """

  alias Manganese.CoreKit
  alias Manganese.RenderKit.Structs

  @typedoc """

  """
  @type t :: %Structs.OrthographicProjectionSet{
    north: CoreKit.Structs.Link.t,
    east: CoreKit.Structs.Link.t,
    south: CoreKit.Structs.Link.t,
    west: CoreKit.Structs.Link.t
  }
  defstruct [
    :north,
    :east,
    :south,
    :west
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "north" => north,
    "east" => east,
    "south" => south,
    "west" => west
  }) do
    %Structs.OrthographicProjectionSet{
      north: CoreKit.Structs.Link.from_map(north),
      east: CoreKit.Structs.Link.from_map(east),
      south: CoreKit.Structs.Link.from_map(south),
      west: CoreKit.Structs.Link.from_map(west)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.OrthographicProjectionSet{
    north: north,
    east: east,
    south: south,
    west: west
  }) do
    %{
      "north" => CoreKit.Structs.Link.to_map(north),
      "east" => CoreKit.Structs.Link.to_map(east),
      "south" => CoreKit.Structs.Link.to_map(south),
      "west" => CoreKit.Structs.Link.to_map(west)
    }
  end


  # Ecto type
  @behaviour Ecto.Type

  @impl Ecto.Type
  def type, do: :map

  @impl Ecto.Type
  def cast(%Structs.OrthographicProjectionSet{} = projection_set), do: { :ok, projection_set }
  def cast(%{} = map), do: { :ok, from_map map }
  def cast(nil), do: { :ok, nil }
  def cast(_), do: :error

  @impl Ecto.Type
  def load(%{} = map), do: { :ok, from_map map }
  def load(nil), do: { :ok, nil }
  def load(_), do: :error

  @impl Ecto.Type
  def dump(%Structs.OrthographicProjectionSet{} = projection_set), do: { :ok, to_map projection_set }
  def dump(nil), do: { :ok, nil }
  def dump(_), do: :error
end