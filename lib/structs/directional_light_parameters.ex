defmodule Manganese.RenderKit.Structs.DirectionalLightParameters do
  @moduledoc """

  """

  alias Manganese.SerializationKit
  alias Manganese.RenderKit.Structs

  @typedoc """

  """
  @type t :: %Structs.DirectionalLightParameters{
    color: SerializationKit.Structs.UnityColor.t,
    intensity: float,
    rotation: SerializationKit.Structs.UnityQuaternion.t
  }
  defstruct [
    :color,
    :intensity,
    :rotation
  ]


  # Deserialization

  @doc """

  """
  @spec from_map(map) :: t
  def from_map(%{
    "color" => color,
    "intensity" => intensity,
    "rotation" => rotation
  }) do
    %Structs.DirectionalLightParameters{
      color: SerializationKit.Structs.UnityColor.from_map(color),
      intensity: intensity,
      rotation: SerializationKit.Structs.UnityQuaternion.from_map(rotation)
    }
  end


  # Serialization

  @doc """

  """
  @spec to_map(t) :: map
  def to_map(%Structs.DirectionalLightParameters{
    color: color,
    intensity: intensity,
    rotation: rotation
  }) do
    %{
      "color" => SerializationKit.Structs.UnityColor.to_map(color),
      "intensity" => intensity,
      "rotation" => SerializationKit.Structs.UnityQuaternion.to_map(rotation)
    }
  end
end